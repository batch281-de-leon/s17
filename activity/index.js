/**
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function printBasicInfo(){
		let fullName = prompt("Enter your full name: ");
		let age = prompt("Enter your age: ");
		let location = prompt("Enter your location:  ");

		console.log("Hello, " + fullName);
		console.log("You are " + age);
		console.log("You live in " + location);
	}; 

	printBasicInfo();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function printFavoriteArtist(){
		let favoriteArtist1 = "Ed Sheeran"
		let favoriteArtist2 = "Taylor Swift"
		let favoriteArtist3 = "Selena Gomez"
		let favoriteArtist4 = "Morissette"
		let favoriteArtist5 = "KZ Tandingan"

		console.log("1. " + favoriteArtist1)
		console.log("2. " + favoriteArtist2)
		console.log("3. " + favoriteArtist3)
		console.log("4. " + favoriteArtist4)
		console.log("5. " + favoriteArtist5)
		
	}

	printFavoriteArtist();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
		let favoriteMovie1 = 'Hunger Games';

		function myFavoriteMovie1(){
		let movieReviews1 = '84%';
		console.log("1. " + favoriteMovie1)
		console.log("Rotten Tomatoes Rating: " + movieReviews1)
		};

	myFavoriteMovie1();

		let favoriteMovie2 = 'The Maze Runner';

		function myFavoriteMovie2(){
		let movieReviews2 = '65%';
		console.log("2. " + favoriteMovie2)
		console.log("Rotten Tomatoes Rating: " + movieReviews2)
		};

	myFavoriteMovie2();

			let favoriteMovie3 = 'Divergent';

		function myFavoriteMovie3(){
		let movieReviews3 = '41%';
		console.log("3. " + favoriteMovie3)
		console.log("Rotten Tomatoes Rating: " + movieReviews3)
		};

	myFavoriteMovie3();

			let favoriteMovie4 = 'The Fault in Our Stars';

		function myFavoriteMovie4(){
		let movieReviews4 = '81%';
		console.log("4. " + favoriteMovie4)
		console.log("Rotten Tomatoes Rating: " + movieReviews4)
		};

	myFavoriteMovie4();

			let favoriteMovie5 = 'Me before you';

		function myFavoriteMovie5(){
		let movieReviews5 = '54%';
		console.log("5. " + favoriteMovie5)
		console.log("Rotten Tomatoes Rating: " + movieReviews5)
		};

	myFavoriteMovie5();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	
	let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
	printFriends();